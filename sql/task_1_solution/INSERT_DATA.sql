insert into user (name, dtm_birth) values ('potopa', '1970-01-01 00:00:00');
insert into user (name, dtm_birth) values ('loboda', '1980-01-01 00:00:00');
insert into product (name, price) values ('parek', 654.00);
insert into product (name, price) values ('parker', 321.00);
insert into product (name, price) values ('karter', 519.00);
insert into user_product (id_user, id_product, dtm) values (1,1, '2013-01-01 00:00:00');
insert into user_product (id_user, id_product, dtm) values (2,2, '2013-01-02 00:00:00');
insert into user_product (id_user, id_product, dtm) values (2,3, '2013-01-03 00:00:00');
insert into user_product (id_user, id_product, dtm) values (1,3, '2013-01-04 00:00:00');
