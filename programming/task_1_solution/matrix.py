h = 1 # height
w = 1 # width
i = 0 # index

class Torus(list):
    def __getitem__(self, n):
        # python only
        try:
            return super(Torus, self).__getitem__(n)
        except IndexError:
            return super(Torus, self).__getitem__(-n)

# Construct matrix
ic = None
matrix = Torus()
for y in range(h):
    matrix.append(Torus())
    for x in range(w):
        v = w*y+x
        matrix[y].append(v)
        # Also get index coordinates
        if i == v: ic = (x,y)

# Compute index coordinates
#ic = (i%w,int(i/w))

# Find neighbours indices
n = []
x = ic[0]
y = ic[1]
for a in (-1,1):
    n.append(matrix[y][x+a])
    n.append(matrix[y+a][x+a])
    n.append(matrix[y+a][x])
    n.append(matrix[y+a][x-a])

print('i =',i)
print('matrix:')
for y in matrix:
    print(y)
print("Neighbours of {i} are {n}.".format(i=i, n=n))
